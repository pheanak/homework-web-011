import React from "react";
import { ListGroup } from "react-bootstrap";

export default function ResultList(props) {
  const { result, value1, value2, operator } = props.result;
  return (
    <ListGroup.Item key={props.index}>
      {"  "} {value1} {operator} {value2} = {result}{"  "}
    </ListGroup.Item>
  );
}
