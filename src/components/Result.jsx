import React from "react";
import { Card, ListGroup } from "react-bootstrap";
import ResultList from "./ShowResult";

export default function Result(props) {
  return (
    <Card>
      <Card.Header>
        <h3>Result History</h3>
      </Card.Header>
      <ListGroup variant="flush">
        {props.results.map((result, index) => (
          <ResultList key={index} result={result} />
        ))}
      </ListGroup>
    </Card>
  );
}
