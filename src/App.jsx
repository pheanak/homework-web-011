import React, { Component } from "react";
import { Card, Form, Container, Row } from "react-bootstrap";
import Result from "./components/Result";
import "./style.css";

export default class App extends Component {
  constructor(props) {
          super(props);
          this.state = { operator: "+", value1: "", value2: "",results: [], };
  }

  handleChange = (event) =>
    this.setState({ [event.target.name]: event.target.value });
  onAdd = () => {
    const { value1, value2, operator } = this.state;

    if (!value1 || !value2) {
      alert("Please input number!!!");
    }
    else if(value1==!Number && value2 == !Number){
      alert("Please input number!!!");
    }
    
    else {
      let result;
      switch (operator) {
        case "+":
          result = Number(value1) + Number(value2);
          break;
        case "-":
          result = Number(value1) - Number(value2);
          break;
        case "*":
          result = Number(value1) * Number(value2);
          break;
        case "/":
          result = Number(value1) / Number(value2);
          break;
        case "%":
          result = Number(value1) % Number(value2);
          break;
        default:
          break;
      }
      this.setState({
        results: [
          ...this.state.results, { result: result,value1: this.state.value1,value2: this.state.value2, operator: this.state.operator, },
                 ],
             });
        }
  };
  render() {
    return (
      <Container>
        <Row>
          <div class="operator col-md-4">
            <Card>
            <Card.Img variant="top" class="img" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTyxjo3A-lUNcp0eJ83LbPx5qtt0Cfgm7SANVbIE9ZvRIWjVeJL&usqp=CAU" />
            <Card.Body>
                <Form.Control class="form" type="text" name="value1" value={this.state.value1} onChange={this.handleChange} />
                <br/>
                <Form.Control class="form" type="text" name="value2" value={this.state.value2} onChange={this.handleChange} />
                <br/>
                <Form.Control class="form" as="select" name="operator" onChange={this.handleChange} >
                  <option value="+">+ Plus</option>
                  <option value="-">- Subtract</option>
                  <option value="*">* Multiply</option>
                  <option value="/">/ Devide</option>
                  <option value="%">% Modulo</option>
                </Form.Control>
                <br/>
                <button class="btn btn-warning" onClick={this.onAdd}>
                  <b>Calculate</b>
                </button>
            </Card.Body>
            </Card>
          </div>
          <div class="result col-md-4">
            <Result results={this.state.results} />
          </div>
        </Row>
      </Container>
    );
  }
}
